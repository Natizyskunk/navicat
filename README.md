# Navicat Premium v12.0.29 x64 - Linux (Wine) Portable.

Updates:
- Navicat Premium v12.0.29.
- Wine-3.0.3 (wine-hq).
- Updated starting script.

Licence:
- Active licence with name "Licenced" & company name "Licenced".

MOD:
- Pre cracked.
- Portable version.
- Work out of the box.